dist:
	tar -czvf Zanko_client.tar.gz .browserslistrc .editorconfig .eslintrc.json .git .gitignore README.md angular.json e2e karma.conf.js package-lock.json package.json pnpm-lock.yaml src tsconfig.app.json tsconfig.json tsconfig.spec.json tslint.json

clean:
	rm -rf ./.idea ./node_modules ./.vscode/
