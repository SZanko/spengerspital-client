import { Component, OnInit } from '@angular/core';
import {
  Patient,
  Patient_GenderCode,
} from '../models/resource/base/individuals/Patient';
import { Period } from '../models/type/data/Period';
import { HumanName, HumanName_UseCode } from '../models/type/data/Humanname';
import {
  ContactPoint,
  ContactPoint_SystemCode,
  ContactPoint_UseCode,
} from '../models/type/data/ContactPoint';
import { CodeableConcept } from '../models/type/data/CodeableConcept';
import { Identifier, Identifier_UseCode } from '../models/type/data/Identifier';
import {
  Address,
  Address_TypeCode,
  Address_UseCode,
} from '../models/type/data/Address';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss'],
})
export class PatientsComponent implements OnInit {
  patientArr$: Patient[];
  selectedPatient: Patient;

  constructor(private service: DataService, private route: ActivatedRoute) {
    this.selectedPatient = new Patient(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  ngOnInit(): void {
    this.getPatients();
    this.selectedPatient.id = this.route.snapshot.paramMap.get('id');
  }

  getPatients(): void {
    this.service.getPatients().subscribe((data: Patient[]) => {
      console.log(data);
      this.patientArr$ = data;
    });
  }

  selectPatient(selected: Patient): void {
    console.log('clicked Patient ' + selected.id);
    this.selectedPatient = selected;
  }

  // Wird von der Patient Component aufgerufen, wenn sich die Liste aktualisieren soll
  onPatientModified(hidePatient: boolean): void {
    console.log('Patient modified ' + hidePatient);
    if (hidePatient) {
      this.selectedPatient = null;
    }
    this.getPatients();
  }

  createPatient(): void {
    const tmpperiod: Period = new Period(new Date('2001-09-11'), new Date());
    const tmphumanname: HumanName = new HumanName(
      'tmpname',
      HumanName_UseCode.temp,
      'tmp-text',
      'tmp-family',
      ['Apple', 'Orange', 'Banana'],
      ['Apple', 'Orange', 'Banana'],
      ['Apple', 'Orange', 'Banana']
    );
    // let tmpcontactpoint: ContactPoint = new ContactPoint('tmpcontactpoint',
    // ContactPoint_SystemCode.sms,'tmpvalue', ContactPoint_UseCode.temp,5,tmpperiod);

    const tmpcodeconcept: CodeableConcept = new CodeableConcept(
      'Tmpcodeconcept',
      null,
      'Test'
    );

    // let tmpidentifier: Identifier = new Identifier('Test', Identifier_UseCode.temp, tmpcodeconcept, 'tmpsystem', 'tmpvalue',tmpperiod);
    const tmpaddress: Address = new Address(
      '4',
      Address_UseCode.temp,
      Address_TypeCode.both,
      'add_text',
      ['add_line'],
      'Vienna',
      'Vienna',
      '1100',
      'Austria',
      tmpperiod
    );

    const newPatient: Patient = new Patient(
      'bla',
      'test',
      [null],
      null,
      null,
      true,
      Patient_GenderCode.unknown,
      new Date(),
      false,
      new Date(),
      false,
      3,
      null
    );
    this.service.addPatient(newPatient).subscribe((patient) => {
      console.log('Patient created');
      this.onPatientModified(true);
    });
  }
}
