import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { ResearchSubject } from '../models/resource/public_health/researchsubject';
import { Identifier, Identifier_UseCode } from '../models/type/data/Identifier';
import { Period } from '../models/type/data/Period';
import { Reference } from '../models/type/data/Reference';

@Component({
  selector: 'app-researchsubject',
  templateUrl: './researchsubject.component.html',
  styleUrls: ['./researchsubject.component.scss'],
})
export class ResearchsubjectComponent implements OnInit {
  researchsubjectArr$: ResearchSubject[];
  selectedResearchSubject: ResearchSubject;

  constructor(private service: DataService, private route: ActivatedRoute) {
    this.selectedResearchSubject = new ResearchSubject(
      null,
      null,
      null,
      null,
      null
    );
  }

  ngOnInit(): void {
    this.getResearchSubjects();
    this.selectedResearchSubject.id = this.route.snapshot.paramMap.get('id');
  }

  getResearchSubjects(): void {
    this.service.getResearchSubjects().subscribe((data: ResearchSubject[]) => {
      console.log(data);
      this.researchsubjectArr$ = data;
    });
  }

  selectResearchSubject(selected: ResearchSubject): void {
    console.log('clicked ResearchSubject ' + selected.id);
    this.selectedResearchSubject = selected;
  }

  // Wird von der ResearchSubject Component aufgerufen, wenn sich die Liste aktualisieren soll
  onResearchSubjectModified(hideResearchSubject: boolean): void {
    console.log('ResearchSubject modified ' + hideResearchSubject);
    if (hideResearchSubject) {
      this.selectedResearchSubject = null;
    }
    this.getResearchSubjects();
  }

  createResearchSubject(): void {
    const tmpperiod: Period = new Period(new Date('2001-09-11'), new Date());

    const tmpidentifier: Identifier = new Identifier(
      '230',
      Identifier_UseCode.temp,
      null,
      'tmpsystem',
      'tmpvalue',
      tmpperiod
    );

    const tmpreference: Reference = new Reference(
      'tmpreference',
      'noreference',
      'notype',
      tmpidentifier,
      'nodisplay'
    );

    const newResearchSubject: ResearchSubject = new ResearchSubject(
      'bla',
      tmpidentifier,
      tmpperiod,
      tmpreference,
      new Date()
    );
    this.service
      .addResearchSubject(newResearchSubject)
      .subscribe((researchsubject) => {
        console.log('ResearchSubject created');
        this.onResearchSubjectModified(true);
      });
  }
}
