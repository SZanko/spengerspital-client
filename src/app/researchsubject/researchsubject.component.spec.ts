import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchsubjectComponent } from './researchsubject.component';

describe('ResearchsubjectComponent', () => {
  let component: ResearchsubjectComponent;
  let fixture: ComponentFixture<ResearchsubjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResearchsubjectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchsubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
