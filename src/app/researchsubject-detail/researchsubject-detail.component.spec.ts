import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchsubjectDetailComponent } from './researchsubject-detail.component';

describe('ResearchsubjectDetailComponent', () => {
  let component: ResearchsubjectDetailComponent;
  let fixture: ComponentFixture<ResearchsubjectDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResearchsubjectDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchsubjectDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
