import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from '../data.service';
import { ResearchSubject } from '../models/resource/public_health/researchsubject';

@Component({
  selector: 'app-researchsubject-detail',
  templateUrl: './researchsubject-detail.component.html',
  styleUrls: ['./researchsubject-detail.component.scss'],
})
export class ResearchsubjectDetailComponent implements OnInit, OnChanges {
  @Input()
  id: string;

  // Notify the parent View to refresh the list
  @Output()
  researchsubjectModified = new EventEmitter<boolean>();

  public researchsubject: ResearchSubject;
  public researchsubjectForm: FormGroup;

  constructor(private service: DataService, private formBuilder: FormBuilder) {
    this.createResearchSubjectForm();
    this.researchsubject = null;
  }

  ngOnInit(): void {
    this.getResearchSubject();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    this.getResearchSubject();
  }

  get namesArray() {
    return this.researchsubjectForm.get('name') as FormArray;
  }

  set namesArray(namesArray: FormArray) {
    this.researchsubjectForm.controls.name = namesArray;
  }

  createResearchSubjectForm(): void {
    this.researchsubjectForm = this.formBuilder.group({
      periodStart: [''],
      periodEnd: [''],
      dateTime: [''],
      display: [''],
    });
  }
  // Wenn ein ResearchSubject vom Server geladen wurde, sollen die ResearchSubjectendaten in das Formular übernommen werden.
  updateResearchSubjectForm(): void {
    this.researchsubjectForm.controls.periodEnd.setValue(
      this.researchsubject.period.end
    );
    this.researchsubjectForm.controls.periodStart.setValue(
      this.researchsubject.period.start
    );
    this.researchsubjectForm.controls.dateTime.setValue(
      this.researchsubject.datetime
    );
    this.researchsubjectForm.controls.display.setValue(
      this.researchsubject.indiviualization.display
    );
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmitUpdate(): void {
    // TODO: Use EventEmitter with form value
    console.log('Update from form data' + this.researchsubjectForm.value);
    this.researchsubject.period.start = this.researchsubjectForm.value.periodStart;
    this.researchsubject.period.end = this.researchsubjectForm.value.periodEnd;
    this.researchsubject.datetime = this.researchsubjectForm.value.dateTime;
    this.researchsubject.indiviualization.display = this.researchsubjectForm.value.display;
    this.updateselectedResearchSubject();
  }

  getResearchSubject(): void {
    this.service
      .getResearchSubjectDetail(this.id)
      .subscribe((data: ResearchSubject) => {
        console.log(data);
        this.researchsubject = data;
        this.updateResearchSubjectForm();
      });
  }

  deleteselectedResearchSubject(): void {
    this.service
      .deleteResearchSubject(this.researchsubject.id)
      .subscribe((x) => this.researchsubjectModified.emit(true));
  }

  updateselectedResearchSubject(): void {
    const newResearchSubject: ResearchSubject = this.researchsubject;
    this.service
      .updateResearchSubject(newResearchSubject)
      .subscribe((researchsubject) => {
        console.log('ResearchSubject updated');
        this.researchsubject = researchsubject;
        this.researchsubjectModified.emit(false);
      });
  }
}
