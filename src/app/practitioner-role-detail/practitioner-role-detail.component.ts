import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from '../data.service';
import {
  DaysOfWeek,
  PractitionerRole,
  PractitionerRole_availableTime,
} from '../models/resource/base/individuals/PractitionerRole';

@Component({
  selector: 'app-practitioner-role-detail',
  templateUrl: './practitioner-role-detail.component.html',
  styleUrls: ['./practitioner-role-detail.component.scss'],
})
export class PractitionerRoleDetailComponent implements OnInit, OnChanges {
  @Input()
  id: string;

  @Output()
  practitionerRoleModified = new EventEmitter<boolean>();

  public practitionerRole: PractitionerRole;
  public practitionerRoleForm: FormGroup;

  constructor(private service: DataService, private formBuilder: FormBuilder) {
    this.createPractitionerRoleForm();
    this.practitionerRole = null;
  }

  ngOnInit(): void {
    this.getPractitionerRole();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    this.getPractitionerRole();
  }

  get atArray() {
    return this.practitionerRoleForm.get('availableTime') as FormArray;
  }
  set atArray(atArray: FormArray) {
    this.practitionerRoleForm.controls.availableTime = atArray;
  }

  get idiArray() {
    return this.practitionerRoleForm.get('identifier') as FormArray;
  }

  set idiArray(idiArray: FormArray) {
    this.practitionerRoleForm.controls.identifier = idiArray;
  }

  createPractitionerRoleForm(): void {
    this.practitionerRoleForm = this.formBuilder.group({
      active: [''],
      periodStart: [''],
      periodEnd: [''],
      identifier: this.formBuilder.array([]),
      availableTime: this.formBuilder.array([]),
    });
  }

  getPractitionerRole(): void {
    this.service
      .getPractitionerRoleDetail(this.id)
      .subscribe((data: PractitionerRole) => {
        console.log(data);
        this.practitionerRole = data;
        this.updatePractitionerRoleForm();
      });
  }

  updatePractitionerRoleForm(): void {
    this.practitionerRoleForm.controls.active.setValue(
      this.practitionerRole.active
    );
    this.practitionerRoleForm.controls.periodEnd.setValue(
      this.practitionerRole.period.end
    );
    this.practitionerRoleForm.controls.periodStart.setValue(
      this.practitionerRole.period.start
    );

    this.clearFormArray(this.idiArray);
    this.practitionerRole.identifier.forEach((identifier) => {
      console.log('pushname ' + identifier.id);
      this.idiArray.push(
        this.formBuilder.group({
          id: [identifier.id],
          value: [identifier.value],
        })
      );
    });

    this.clearFormArray(this.atArray);
    this.practitionerRole.availableTime.forEach((availableTime) => {
      console.log('pushname ' + availableTime.id);
      this.atArray.push(
        this.formBuilder.group({
          id: [availableTime.id],
          daysOfWeek: [availableTime.daysOfWeek],
          allDay: [availableTime.allDay],
          availableStartTime: [availableTime.availableStartTime],
          availableEndTime: [availableTime.availableEndTime],
        })
      );
    });
  }

  public addIdentifier(): void {
    this.idiArray.push(this.createIdentifier());
  }

  createIdentifier(): FormGroup {
    return this.formBuilder.group({
      id: [''],
      value: [''],
    });
  }

  public addAvailableTime(): void {
    this.atArray.push(this.createAvailableTime());
  }

  createAvailableTime(): FormGroup {
    return this.formBuilder.group({
      id: [''],
      daysOfWeek: [['tue', 'wed']],
      allDay: false,
      availableStartTime: '2021-02-20T11:54:00',
      availableEndTime: '2021-02-20T11:54:00',
    });
  }

  onSubmitUpdate() {
    console.log('Update from formdata' + this.practitionerRoleForm.value);
    this.practitionerRole.period.start = this.practitionerRoleForm.value.periodStart;
    this.practitionerRole.period.end = this.practitionerRoleForm.value.periodEnd;
    this.practitionerRole.identifier = this.practitionerRoleForm.value.identifier;
    //    this.practitionerRole.availableTime = [
    //      this.practitionerRoleForm.value.availableTime,
    //    ];
    // this.practitionerRole.availableTime = new PractitionerRole_availableTime(
    //   null,
    //   [DaysOfWeek.mon],
    //   false,
    //   new Date(),
    //   new Date()
    // );
    //
    //this.practitionerRole.other.reference = this.practitionerRoleForm.value.reference;
    this.updatePractitionerRole();
  }

  updatePractitionerRole() {
    const newPractitionerRole: PractitionerRole = this.practitionerRole;
    this.service
      .updatePractitionerRole(newPractitionerRole)
      .subscribe((practitionerRole) => {
        console.log('PractitionerRole updated');
        this.practitionerRole = practitionerRole;
        this.practitionerRoleModified.emit(false);
      });
  }

  getPractitionerRoleDetail() {
    this.service
      .getPractitionerRoleDetail(this.id)
      .subscribe((data: PractitionerRole) => {
        console.log(data);
        this.practitionerRole = data;
        this.updatePractitionerRoleForm();
      });
  }

  deletePractitionerRole() {
    this.service
      .deletePractitionerRole(this.practitionerRole.id)
      .subscribe((x) => this.practitionerRoleModified.emit(true));
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };
}
