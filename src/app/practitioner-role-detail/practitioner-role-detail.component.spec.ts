import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PractitionerRoleDetailComponent } from './practitioner-role-detail.component';

describe('PractitionerRoleDetailComponent', () => {
  let component: PractitionerRoleDetailComponent;
  let fixture: ComponentFixture<PractitionerRoleDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PractitionerRoleDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PractitionerRoleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
