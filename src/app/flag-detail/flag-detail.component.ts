import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { DataService } from '../data.service';
import { Flag, FlagStatus } from '../models/type/data/Flag';
import { Period } from '../models/type/data/Period';

@Component({
  selector: 'app-flag-detail',
  templateUrl: './flag-detail.component.html',
  styleUrls: ['./flag-detail.component.scss'],
})
export class FlagDetailComponent implements OnInit, OnChanges {
  @Input()
  id: string;
  // Notify the parent View to refresh the list
  @Output()
  flagModified = new EventEmitter<boolean>();

  public flag: Flag;
  public flagForm: FormGroup;

  constructor(private service: DataService, private formBuilder: FormBuilder) {
    this.createFlagForm();
    this.flag = null;
  }

  ngOnInit(): void {
    this.getFlag();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    this.getFlag();
  }

  createFlagForm(): void {
    this.flagForm = this.formBuilder.group({
      status: ['activity'],
      periodStart: [''],
      periodEnd: [''],
    });
  }

  // Wenn ein Flag vom Server geladen wurde, sollen die Flagendaten in das Formular übernommen werden.
  updateFlagForm(): void {
    this.flagForm.controls.status.setValue(this.flag.status);
    this.flagForm.controls.periodEnd.setValue(this.flag.period.end);
    this.flagForm.controls.periodStart.setValue(this.flag.period.start);
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmitUpdate(): void {
    // TODO: Use EventEmitter with form value
    console.log('Update from form data' + this.flagForm.value);
    this.flag.status = this.flagForm.value.status;
    this.flag.period = this.flagForm.value.period;
    this.updateFlag();
  }

  getFlag(): void {
    this.service.getFlagDetail(this.id).subscribe((data: Flag) => {
      console.log(data);
      this.flag = data;
      this.updateFlagForm();
    });
  }

  deleteselectedFlag(): void {
    this.service
      .deleteFlag(this.flag.id)
      .subscribe((x) => this.flagModified.emit(true));
  }
  updateFlag(): void {
    const newFlag: Flag = this.flag;
    this.service.updateFlag(newFlag).subscribe((flag) => {
      console.log('Flag updated');
      this.flag = flag;
      this.flagModified.emit(false);
    });
  }
}
