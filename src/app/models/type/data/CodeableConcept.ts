import {Coding} from './Coding';

export class CodeableConcept{
	constructor(
		public id: string,
		public coding: [Coding],
		public text: string
	){}
}
