export class Coding{
	constructor(
		public id: string,
		public c_system: string,
		public c_code: string,
		public c_version: string,
		public c_display: string,
		public c_userSelected: boolean
	){}
}
