import { Identifier } from './Identifier';
import { Reference } from './Reference';
import { Period } from './Period';

export class Flag {
  constructor(
    public id: string,
    public identifier: Identifier,
    public author: Reference,
    public status: FlagStatus,
    public period: Period
  ) {}
}

export enum FlagStatus {
  activity = 'activity',
  inactivity = 'inactivity',
  enteredplusend = 'entered+end',
}
