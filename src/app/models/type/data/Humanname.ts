export class HumanName {
  constructor(
    public id: string = '',
    public use: HumanName_UseCode,
    public text: string,
    public family: string,
    public given: string[],
    public prefix: string[],
    public suffix: string[]
  ) {
    this.id = id;
    this.use = use;
    this.family = family;
    this.text = text;
    this.family = family;
    this.given = given;
    this.prefix = prefix;
    this.suffix = suffix;
  }
}
export enum HumanName_UseCode {
  usual,
  official,
  temp,
  nickname,
  anonymous,
  old,
  maiden,
}
