import { Period } from './Period';
import { CodeableConcept } from './CodeableConcept';


export class Identifier{
	constructor(
		public id: string,
		public code: Identifier_UseCode,
		public type: CodeableConcept,
		public system: string,
		public value: string,
		public period: Period
	){}
}

export enum Identifier_UseCode{
	usual='usual',
	official='official',
	temp='temp',
	secondary='secondary',
	old='old'
}
