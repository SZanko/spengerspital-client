import { Identifier } from './Identifier';

export class Reference {
  display: string;
  reference: string;

  constructor(
    id: string,
    reference: string,
    type: string,
    identifier: Identifier,
    display: string
  ) {
    this.display = display;
    this.reference = reference;
  }
}
