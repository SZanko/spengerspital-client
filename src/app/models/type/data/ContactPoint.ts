import { Period } from './Period';

export class ContactPoint{
	constructor(
		public id: string,
		public system: ContactPoint_SystemCode,
		public value: string,
		public use: ContactPoint_UseCode,
		public rank: number,
		public period: Period
	){
		this.id = id;
		this.system = system;
		this.value = value;
		this.use = use;
		this.rank = rank;
		this.period = period;
	}
}
export enum ContactPoint_SystemCode{
	phone='phone',
	fax='fax',
	email='email',
	pager='pager',
	url='url',
	sms='sms',
	other='other'
}
export enum ContactPoint_UseCode{
	home='home',
	work='work',
	temp='temp',
	old='old',
	mobile='mobile'
}
