export class Period {
  public start: Date;
  public end: Date;
  constructor(start: Date, end: Date) {
    this.end = end;
    this.start = start;
  }
}
