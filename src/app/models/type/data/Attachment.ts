export class Attachment{
	constructor(
		public id: string
	){}
}

enum LanguageCode{
	ar,
	bn,
	cs,
	da,
	de
}
