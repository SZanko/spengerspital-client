import { Period } from './Period';

export class Address {
  constructor(
    public id: string,
    public add_use: Address_UseCode,
    public add_type: Address_TypeCode,
    public add_text: string,
    public add_line: [string],
    public add_city: string,
    public add_state: string,
    public add_postalCode: string,
    public add_country: string,
    public add_period: Period
  ) {}
}

export enum Address_UseCode {
  home = 'home',
  work = 'work',
  temp = 'temp',
  old = 'old',
  billing = 'billing',
}

export enum Address_TypeCode {
  postal = 'postal',
  physical = 'physical',
  both = 'both',
}
