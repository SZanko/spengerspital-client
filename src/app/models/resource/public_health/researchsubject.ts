import { Identifier } from '../../type/data/Identifier';
import { Period } from '../../type/data/Period';
import { Reference } from '../../type/data/Reference';

export class ResearchSubject {
  constructor(
    public id: string,
    public identifier: Identifier,
    public period: Period,
    public indiviualization: Reference,
    public datetime: Date
  ) {}
}
