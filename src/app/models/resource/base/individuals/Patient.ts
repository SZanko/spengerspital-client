import { HumanName } from '../../../type/data/Humanname';
import { ContactPoint } from '../../../type/data/ContactPoint';
import { Identifier } from '../../../type/data/Identifier';
import { Address } from '../../../type/data/Address';

export class Patient {
  constructor(
    public id: string,
    public resourceType: string,
    public identifier: [Identifier],
    public name: [HumanName], // A name associated with the patient
    public telecom: [ContactPoint],
    public active: boolean,
    public gender: Patient_GenderCode,
    public birthDate: Date,
    public deceasedBoolean: boolean,
    public deceasedDateTime: Date,
    public multipleBirthBoolean: boolean,
    public multipleBirthInteger: number,
    public address: [Address]
  ) {}
}

export enum Patient_GenderCode {
  male = 'male',
  female = 'female',
  other = 'other',
  unknown = 'unknown',
}
