import { HumanName } from '../../../type/data/Humanname';
import { Period } from '../../../type/data/Period';
import { ContactPoint } from '../../../type/data/ContactPoint';
import { Address } from '../../../type/data/Address';
import { Identifier } from '../../../type/data/Identifier';
import { Attachment } from '../../../type/data/Attachment';
import { CodeableConcept } from '../../../type/data/CodeableConcept';

export class Practitioner {
  constructor(
    public id: string,
    public identifier: [Identifier],
    public active: boolean,
    public name: [HumanName],
    public telecom: [ContactPoint],
    public address: [Address],
    public gender: PractitionerGenderCode,
    public birthDate: Date,
    public photo: [Attachment],
    public qualification: [PractitionerQualifiction],
    public communication: [CodeableConcept]
  ) {}
}
export enum PractitionerGenderCode {
  male = 'male',
  female = 'female',
  other = 'other',
  unknown = 'unknown',
}
class PractitionerQualifiction {
  constructor(
    public identifier: [Identifier],
    public code: CodeableConcept,
    public quali_period: Period
  ) {}
}
