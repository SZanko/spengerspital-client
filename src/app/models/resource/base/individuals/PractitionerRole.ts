import { Period } from '../../../type/data/Period';
import { Identifier } from '../../../type/data/Identifier';
import { ContactPoint } from '../../../type/data/ContactPoint';
import { Reference } from '../../../type/data/Reference';
import { CodeableConcept } from '../../../type/data/CodeableConcept';

export class PractitionerRole {
  constructor(
    public id: string,
    public identifier: Identifier[],
    public active: boolean,
    public period: Period,
    public practitioner: Reference,
    public organization: Reference,
    public code: CodeableConcept[],
    public specialty: CodeableConcept[],
    public location: Reference[],
    public healthcareService: Reference[],
    public telecom: ContactPoint[],
    public availableTime: PractitionerRole_availableTime[]
  ) {}
}

export class PractitionerRole_availableTime {
  constructor(
    public id: string,
    public daysOfWeek: [DaysOfWeek],
    public allDay: boolean,
    public availableStartTime: Date,
    public availableEndTime: Date
  ) {}
}

export enum DaysOfWeek {
  mon = 'mon',
  tue = 'tue',
  wed = 'wed',
  thu = 'thu',
  fri = 'fri',
  sat = 'sat',
  sun = 'sun',
}
