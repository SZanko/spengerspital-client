import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Flag, FlagStatus } from '../models/type/data/Flag';

@Component({
  selector: 'app-flags',
  templateUrl: './flags.component.html',
  styleUrls: ['./flags.component.scss'],
})
export class FlagsComponent implements OnInit {
  flagArr$: Flag[];
  selectedFlag: Flag;

  constructor(private service: DataService) {}

  ngOnInit(): void {
    this.getFlags();
  }

  getFlags(): void {
    this.service.getFlags().subscribe((data: Flag[]) => {
      console.log(data);
      this.flagArr$ = data;
    });
  }

  selectFlag(selected: Flag): void {
    console.log('clicked Flag' + selected.id);
    this.selectedFlag = selected;
  }

  // Wird von der Flag Component aufgerufen, wenn sich die Liste aktualisieren soll
  onFlagModified(hideFlag: boolean): void {
    console.log('Flag modified ' + hideFlag);
    if (hideFlag) {
      this.selectedFlag = null;
    }
    this.getFlags();
  }

  createFlag(): void {
    const newFlag: Flag = new Flag(
      'Test123',
      null,
      null,
      FlagStatus.activity,
      null
    );
    this.service.addFlag(newFlag).subscribe((flag) => {
      console.log('Flag created');
      this.onFlagModified(true);
    });
  }
}
