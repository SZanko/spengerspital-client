import { Component, OnInit } from '@angular/core';
import {
  Practitioner,
  PractitionerGenderCode,
} from '../models/resource/base/individuals/Practitioner';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-practitioners',
  templateUrl: './practitioners.component.html',
  styleUrls: ['./practitioners.component.scss'],
})
export class PractitionersComponent implements OnInit {
  practitionerArr$: Practitioner[];
  selectedPractitioner: Practitioner;

  constructor(private service: DataService, private route: ActivatedRoute) {
    this.selectedPractitioner = new Practitioner(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  ngOnInit(): void {
    this.getPractitioners();
    this.selectedPractitioner.id = this.route.snapshot.paramMap.get('id');
  }

  getPractitioners(): void {
    this.service.getPractitioners().subscribe((data: Practitioner[]) => {
      this.practitionerArr$ = data;
    });
  }

  selectPractitioner(selected: Practitioner): void {
    console.log('clicked Practitioner' + selected.id);
    this.selectedPractitioner = selected;
  }

  // Wird von der Practitioner Component aufgerufen, wenn sich die Liste aktualisieren soll
  onPractitionerModified(hidePractitioner: boolean): void {
    console.log('Practitioner modified ' + hidePractitioner);
    if (hidePractitioner) {
      this.selectedPractitioner = null;
    }
    this.getPractitioners();
  }

  createPractitioner(): void {
    const newPractitioner: Practitioner = new Practitioner(
      'Test',
      [null],
      false,
      null,
      null,
      null,
      PractitionerGenderCode.unknown,
      new Date('2000-02-02'),
      null,
      null,
      null
    );
    this.service.addPractitioner(newPractitioner).subscribe((patient) => {
      console.log('Practitioner created');
      this.onPractitionerModified(true);
    });
  }
}
