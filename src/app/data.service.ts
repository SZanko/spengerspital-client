import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ResearchSubject } from './models/resource/public_health/researchsubject';
import { PractitionerRole } from './models/resource/base/individuals/PractitionerRole';
import { Patient } from './models/resource/base/individuals/Patient';
import { Practitioner } from './models/resource/base/individuals/Practitioner';
import { Flag } from './models/type/data/Flag';

const flagUrl = 'https://localhost:8080/api/flag/';
const patientUrl = 'https://localhost:8080/api/patient/';
const practitionerroleUrl = 'https://localhost:8080/api/practitionerrole/';
const practitionerUrl = 'https://localhost:8080/api/practitioner/';
const researchsubjectUrl = 'https://localhost:8080/api/researchsubject/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {}

  // Flag
  public getFlags(): Observable<Flag[]> {
    console.log('getFlags called');
    return this.http
      .get<Flag[]>(flagUrl)
      .pipe(catchError(this.handleError('getFlag', [])));
  }

  public getFlagDetail(id: string): Observable<Flag> {
    return this.http
      .get<Flag>(flagUrl + id)
      .pipe(catchError(this.handleError('getFlagDetail', null)));
  }

  public deleteFlag(id: string): Observable<Flag> {
    return this.http
      .delete(flagUrl + id, httpOptions)
      .pipe(catchError(this.handleError('delete Flag')));
  }

  public addFlag(flag: Flag): Observable<Flag> {
    return this.http.post<Flag>(flagUrl, flag, httpOptions);
  }

  public updateFlag(flag: Flag): Observable<Flag> {
    return this.http
      .put<Flag>(flagUrl + flag.id, flag, httpOptions)
      .pipe(catchError(this.handleError('updateFlag', flag)));
  }

  // Patient
  public getPatients(): Observable<Patient[]> {
    console.log('getPatients called');
    return this.http
      .get<Patient[]>(patientUrl)
      .pipe(catchError(this.handleError('getPatient', [])));
  }

  public getPatientDetail(id: string): Observable<Patient> {
    return this.http
      .get<Patient>(patientUrl + '/' + id)
      .pipe(catchError(this.handleError('getPatientDetail', null)));
  }

  public deletePatient(id: string): Observable<Patient> {
    return this.http
      .delete(patientUrl + id, httpOptions)
      .pipe(catchError(this.handleError('delete Patient')));
  }

  public addPatient(patient: Patient): Observable<Patient> {
    return this.http.post<Patient>(patientUrl, patient, httpOptions);
  }

  public updatePatient(patient: Patient): Observable<Patient> {
    return this.http
      .put<Patient>(patientUrl + patient.id, patient, httpOptions)
      .pipe(catchError(this.handleError('updatePatient', patient)));
  }

  // Practitioner
  public getPractitioners(): Observable<Practitioner[]> {
    console.log('getPractitioners called');
    return this.http
      .get<Practitioner[]>(practitionerUrl)
      .pipe(catchError(this.handleError('getPractitioner', [])));
  }

  public getPractitionerDetail(id: string): Observable<Practitioner> {
    return this.http
      .get<Practitioner>(practitionerUrl + id)
      .pipe(catchError(this.handleError('getPractitionerDetail', null)));
  }

  public deletePractitioner(id: string): Observable<Practitioner> {
    return this.http
      .delete(practitionerUrl + id, httpOptions)
      .pipe(catchError(this.handleError('delete Practitioner')));
  }

  public addPractitioner(practitioner: Practitioner): Observable<Practitioner> {
    return this.http.post<Practitioner>(
      practitionerUrl,
      practitioner,
      httpOptions
    );
  }

  public updatePractitioner(
    practitioner: Practitioner
  ): Observable<Practitioner> {
    return this.http
      .put<Practitioner>(
        practitionerUrl + practitioner.id,
        practitioner,
        httpOptions
      )
      .pipe(catchError(this.handleError('updatePractitioner', practitioner)));
  }

  // ResearchSubject
  public getResearchSubjects(): Observable<ResearchSubject[]> {
    console.log('getResearchSubjects called');
    return this.http
      .get<ResearchSubject[]>(researchsubjectUrl)
      .pipe(catchError(this.handleError('getResearchSubject', [])));
  }

  public getResearchSubjectDetail(id: string): Observable<ResearchSubject> {
    return this.http
      .get<ResearchSubject>(researchsubjectUrl + id)
      .pipe(catchError(this.handleError('getResearchSubjectDetail', null)));
  }

  public deleteResearchSubject(id: string): Observable<ResearchSubject> {
    return this.http
      .delete(researchsubjectUrl + id, httpOptions)
      .pipe(catchError(this.handleError('delete ResearchSubject')));
  }

  public updateResearchSubject(
    researchsubject: ResearchSubject
  ): Observable<ResearchSubject> {
    return this.http
      .put<ResearchSubject>(
        researchsubjectUrl + researchsubject.id,
        researchsubject,
        httpOptions
      )
      .pipe(catchError(this.handleError('updateFlag', researchsubject)));
  }

  public addResearchSubject(
    researchsubject: ResearchSubject
  ): Observable<ResearchSubject> {
    return this.http.post<ResearchSubject>(
      researchsubjectUrl,
      researchsubject,
      httpOptions
    );
  }

  // PractitionerRole
  public getPractitionerRoles(): Observable<PractitionerRole[]> {
    console.log('getPractitionerRoles called');
    return this.http
      .get<PractitionerRole[]>(practitionerroleUrl)
      .pipe(catchError(this.handleError('getPractitionerRole', [])));
  }

  public getPractitionerRoleDetail(id: string): Observable<PractitionerRole> {
    return this.http
      .get<PractitionerRole>(practitionerroleUrl + '/' + id)
      .pipe(catchError(this.handleError('getPractitionerRoleDetail', null)));
  }

  public deletePractitionerRole(id: string): Observable<PractitionerRole> {
    return this.http
      .delete(practitionerroleUrl + id, httpOptions)
      .pipe(catchError(this.handleError('delete PractitionerRole')));
  }

  public addPractitionerRole(
    practitionerrole: PractitionerRole
  ): Observable<PractitionerRole> {
    return this.http.post<PractitionerRole>(
      practitionerroleUrl,
      practitionerrole,
      httpOptions
    );
  }

  public updatePractitionerRole(
    practitionerrole: PractitionerRole
  ): Observable<PractitionerRole> {
    return this.http
      .put<PractitionerRole>(
        practitionerroleUrl + practitionerrole.id,
        practitionerrole,
        httpOptions
      )
      .pipe(
        catchError(this.handleError('updatePractitionerRole', practitionerrole))
      );
  }

  private handleError<T>(operation = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
