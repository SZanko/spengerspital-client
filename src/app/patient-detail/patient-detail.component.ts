import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import {
  Patient,
  Patient_GenderCode,
} from '../models/resource/base/individuals/Patient';
import { HumanName } from '../models/type/data/Humanname';
import { DataService } from '../data.service';

@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.component.html',
  styleUrls: ['./patient-detail.component.scss'],
})
export class PatientDetailComponent implements OnInit, OnChanges {
  @Input()
  id: string;

  @Output()
  patientModified = new EventEmitter<boolean>();

  public patient: Patient;
  public patientForm: FormGroup;

  constructor(private service: DataService, private formBuilder: FormBuilder) {
    this.createPatientForm();
    this.patient = null;
  }

  // Notify the parent View to refresh the list

  ngOnInit(): void {
    this.getPatient();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    this.getPatient();
  }

  get namesArray() {
    return this.patientForm.get('name') as FormArray;
  }

  set namesArray(namesArray: FormArray) {
    this.patientForm.controls.name = namesArray;
  }

  createPatientForm(): void {
    this.patientForm = this.formBuilder.group({
      active: [''],
      gender: [Patient_GenderCode.unknown],
      deceasedBoolean: [''],
      deceasedDateTime: [''],
      birthDate: [''],
      name: this.formBuilder.array([]),
    });
  }
  // Wenn ein Patient vom Server geladen wurde, sollen die Patientendaten in das Formular übernommen werden.
  updatePatientForm(): void {
    this.patientForm.controls.active.setValue(this.patient.active);
    this.patientForm.controls.gender.setValue(this.patient.gender);
    this.patientForm.controls.deceasedDateTime.setValue(
      this.patient.deceasedDateTime
    );
    this.patientForm.controls.deceasedBoolean.setValue(
      this.patient.deceasedBoolean
    );
    this.patientForm.controls.birthDate.setValue(this.patient.birthDate);

    this.clearFormArray(this.namesArray);

    this.patient.name.forEach((name) => {
      console.log('push name' + name.family);
      this.namesArray.push(
        this.formBuilder.group({
          id: [name.id],
          text: [name.text],
          use: [name.use],
          family: [name.family],
        })
      );
    });
  }
  // Einen Namen hinzufügen
  public addName(): void {
    this.namesArray.push(this.createName());
  }

  createName(): FormGroup {
    return this.formBuilder.group({
      id: [''],
      use: ['official'],
      text: [''],
      family: [''],
    });
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmitUpdate(): void {
    // TODO: Use EventEmitter with form value
    console.log('Update from form data' + this.patientForm.value);
    this.patient.active = this.patientForm.value.active;
    this.patient.gender = this.patientForm.value.gender;
    this.patient.birthDate = this.patientForm.value.birthDate;
    this.patient.deceasedDateTime = this.patientForm.value.deceasedDateTime;
    this.patient.deceasedBoolean = this.patientForm.value.deceasedBoolean;
    this.patient.name = this.patientForm.value.name;
    this.updatePatient();
  }

  getPatient(): void {
    this.service.getPatientDetail(this.id).subscribe((data: Patient) => {
      console.log(data);
      this.patient = data;
      this.updatePatientForm();
    });
  }

  deletePatient(): void {
    this.service
      .deletePatient(this.patient.id)
      .subscribe((x) => this.patientModified.emit(true));
  }

  updatePatient(): void {
    const newPatient: Patient = this.patient;
    this.service.updatePatient(newPatient).subscribe((patient) => {
      console.log('Patient updated');
      this.patient = patient;
      this.patientModified.emit(false);
    });
  }
}
