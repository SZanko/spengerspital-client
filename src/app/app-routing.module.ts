import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientDetailComponent } from './patient-detail/patient-detail.component';
import { PatientsComponent } from './patients/patients.component';
import { PractitionersComponent } from './practitioners/practitioners.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FlagsComponent } from './flags/flags.component';
import { PractitionerRolesComponent } from './practitioner-roles/practitioner-roles.component';
import { ResearchsubjectComponent } from './researchsubject/researchsubject.component';

const routes: Routes = [
  {
    path: '',
    component: PatientsComponent,
  },
  {
    path: 'patients',
    component: PatientsComponent,
  },
  {
    path: 'patients/:id',
    component: PatientsComponent,
  },
  {
    path: 'practitioners',
    component: PractitionersComponent,
  },
  {
    path: 'practitioners/:id',
    component: PractitionersComponent,
  },
  {
    path: 'flags',
    component: FlagsComponent,
  },
  {
    path: 'flags/:id',
    component: FlagsComponent,
  },
  {
    path: 'practitioner-roles',
    component: PractitionerRolesComponent,
  },
  {
    path: 'practitioner-roles/:id',
    component: PractitionerRolesComponent,
  },
  {
    path: 'research-subject',
    component: ResearchsubjectComponent,
  },
  {
    path: 'research-subject/:id',
    component: ResearchsubjectComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
