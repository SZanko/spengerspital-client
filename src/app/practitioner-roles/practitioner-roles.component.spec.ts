import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PractitionerRolesComponent } from './practitioner-roles.component';

describe('PractitionerRolesComponent', () => {
  let component: PractitionerRolesComponent;
  let fixture: ComponentFixture<PractitionerRolesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PractitionerRolesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PractitionerRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
