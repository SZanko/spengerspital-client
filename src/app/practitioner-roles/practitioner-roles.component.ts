import { Component, OnInit } from '@angular/core';
import { Period } from '../models/type/data/Period';
import {
  ContactPoint,
  ContactPoint_SystemCode,
  ContactPoint_UseCode,
} from '../models/type/data/ContactPoint';
import {
  DaysOfWeek,
  PractitionerRole,
  PractitionerRole_availableTime,
} from '../models/resource/base/individuals/PractitionerRole';
import { CodeableConcept } from '../models/type/data/CodeableConcept';
import { Identifier, Identifier_UseCode } from '../models/type/data/Identifier';
import { Reference } from '../models/type/data/Reference';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-practitioner-roles',
  templateUrl: './practitioner-roles.component.html',
  styleUrls: ['./practitioner-roles.component.scss'],
})
export class PractitionerRolesComponent implements OnInit {
  practitionerroleArr$: PractitionerRole[];
  selectedPractitionerRole: PractitionerRole;

  constructor(private service: DataService, private route: ActivatedRoute) {
    this.selectedPractitionerRole = new PractitionerRole(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  ngOnInit(): void {
    this.getPractitionerRoles();
    this.selectedPractitionerRole.id = this.route.snapshot.paramMap.get('id');
  }

  getPractitionerRoles(): void {
    this.service
      .getPractitionerRoles()
      .subscribe((data: PractitionerRole[]) => {
        console.log(data);
        this.practitionerroleArr$ = data;
      });
  }
  selectPractitionerRole(selected: PractitionerRole): void {
    console.log('clicked PractitionerRole ' + selected.id);
    this.selectedPractitionerRole = selected;
  }

  // Wird von der PractitionerRole Component aufgerufen, wenn sich die Liste aktualisieren soll
  onPractitionerRoleModified(hidePractitionerRole: boolean): void {
    console.log('PractitionerRole modified ' + hidePractitionerRole);
    if (hidePractitionerRole) {
      this.selectedPractitionerRole = null;
    }
    this.getPractitionerRoles();
  }

  createPractitionerRole(): void {
    const tmpperiod: Period = new Period(new Date('2001-09-11'), new Date());
    const tmpcontactpoint: ContactPoint = new ContactPoint(
      'tmpcontactpoint',
      ContactPoint_SystemCode.sms,
      'tmpvalue',
      ContactPoint_UseCode.temp,
      5,
      tmpperiod
    );

    const tmpcodeconcept: CodeableConcept = new CodeableConcept(
      'Tmpcodeconcept',
      null,
      'Test'
    );

    const tmpidentifier: Identifier = new Identifier(
      'Test',
      Identifier_UseCode.temp,
      tmpcodeconcept,
      'tmpsystem',
      'tmpvalue',
      tmpperiod
    );
    const tmpreference: Reference = new Reference(
      'tmpreference',
      'hanswurst',
      'tmptype',
      tmpidentifier,
      'nein'
    );

    const tmpPractitionerroleTime: PractitionerRole_availableTime = new PractitionerRole_availableTime(
      'tmprole',
      [DaysOfWeek.fri],
      true,
      new Date(),
      new Date()
    );

    const newPractitionerRole: PractitionerRole = new PractitionerRole(
      'bla',
      //[tmpidentifier],
      null,
      true,
      tmpperiod,
      null,
      null,
      //[tmpcodeconcept],
      null,
      null,
      //[tmpcodeconcept],
      null,
      null,
      //[tmpcontactpoint],
      null,
      //[tmpPractitionerrole_time]
      null
    );
    this.service
      .addPractitionerRole(newPractitionerRole)
      .subscribe((patient) => {
        console.log('PractitionerRole created');
        this.onPractitionerRoleModified(true);
      });
  }
}
