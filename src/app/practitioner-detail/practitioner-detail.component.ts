import {
  Component,
  OnInit,
  OnChanges,
  Output,
  Input,
  EventEmitter,
} from '@angular/core';
import {
  Practitioner,
  PractitionerGenderCode,
} from '../models/resource/base/individuals/Practitioner';
import {
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
  FormGroup,
} from '@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-practitioner-detail',
  templateUrl: './practitioner-detail.component.html',
  styleUrls: ['./practitioner-detail.component.scss'],
})
export class PractitionerDetailComponent implements OnInit, OnChanges {
  @Input()
  id: string;

  // Notify the parent View to refresh the list
  @Output()
  practitionerModified = new EventEmitter<boolean>();

  public practitioner: Practitioner = null;
  public practitionerForm: FormGroup;

  constructor(private service: DataService, private formBuilder: FormBuilder) {
    this.createPractitionerForm();
    this.practitioner = null;
  }

  ngOnInit(): void {
    this.getPractitioner();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    this.getPractitioner();
  }

  get qualificationArray(): FormArray {
    return this.practitionerForm.get('qualification') as FormArray;
  }

  set qualificationArray(qualificationArray: FormArray) {
    this.practitionerForm.controls.qualification = qualificationArray;
  }

  get namesArray(): FormArray {
    return this.practitionerForm.get('name') as FormArray;
  }

  set namesArray(namesArray: FormArray) {
    this.practitionerForm.controls.name = namesArray;
  }

  createPractitionerForm(): void {
    this.practitionerForm = this.formBuilder.group({
      active: [''],
      gender: [PractitionerGenderCode.unknown],
      qualification: this.formBuilder.array([]),
      deceasedDateTime: [''],
      birthDate: [''],
      name: this.formBuilder.array([]),
    });
  }

  updatePractitionerForm(): void {
    this.practitionerForm.controls.active.setValue(this.practitioner.active);
    this.practitionerForm.controls.gender.setValue(this.practitioner.gender);
    // this.practitionerForm.controls.
    // this.practitionerForm.controls.deceasedBoolean.setValue(
    //   this.practitioner.deceasedBoolean
    // );
    this.practitionerForm.controls.birthDate.setValue(
      this.practitioner.birthDate
    );

    this.clearFormArray(this.namesArray);
    this.clearFormArray(this.qualificationArray);

    this.practitioner.qualification.forEach((quali) => {
      console.log('push qualification' + quali.identifier);
      this.qualificationArray.push(
        this.formBuilder.group({
          identifier: [quali.identifier],
          code: [quali.code],
          period: [quali.quali_period],
        })
      );
    });

    this.practitioner.name.forEach((name) => {
      console.log('push name' + name.family);
      this.namesArray.push(
        this.formBuilder.group({
          id: [name.id],
          text: [name.text],
          use: [name.use],
          family: [name.family],
        })
      );
    });
  }

  // Einen Namen hinzufügen
  public addName(): void {
    this.namesArray.push(this.createName());
  }

  public addQualification(): void {
    this.qualificationArray.push(this.createQualification());
  }

  createName(): FormGroup {
    return this.formBuilder.group({
      id: [''],
      use: ['official'],
      text: [''],
      family: [''],
    });
  }

  createQualification(): FormGroup {
    return this.formBuilder.group({
      identifier: [''],
      code: [''],
      period: [''],
    });
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmitUpdate(): void {
    // TODO: Use EventEmitter with form value
    console.log('Update from form data' + this.practitionerForm.value);
    this.practitioner.active = this.practitionerForm.value.active;
    this.practitioner.gender = this.practitionerForm.value.gender;
    this.practitioner.birthDate = this.practitionerForm.value.birthDate;
    this.practitioner.name = this.practitionerForm.value.name;
    this.updatePractitioner();
  }

  getPractitioner(): void {
    this.service
      .getPractitionerDetail(this.id)
      .subscribe((data: Practitioner) => {
        console.log(data);
        this.practitioner = data;
        this.updatePractitionerForm();
      });
  }

  deletePractitioner(): void {
    this.service
      .deletePractitioner(this.practitioner.id)
      .subscribe((x) => this.practitionerModified.emit(true));
  }

  updatePractitioner(): void {
    const newPractitioner: Practitioner = this.practitioner;
    this.service
      .updatePractitioner(newPractitioner)
      .subscribe((practitioner) => {
        console.log('Practitioner updated');
        this.practitioner = practitioner;
        this.practitionerModified.emit(false);
      });
  }
}
