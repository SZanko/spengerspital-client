import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PatientsComponent } from './patients/patients.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HttpClientModule } from '@angular/common/http';
import { PatientDetailComponent } from './patient-detail/patient-detail.component';
import { PractitionersComponent } from './practitioners/practitioners.component';
import { PractitionerDetailComponent } from './practitioner-detail/practitioner-detail.component';
import { FlagsComponent } from './flags/flags.component';
import { FlagDetailComponent } from './flag-detail/flag-detail.component';
import { PractitionerRolesComponent } from './practitioner-roles/practitioner-roles.component';
import { PractitionerRoleDetailComponent } from './practitioner-role-detail/practitioner-role-detail.component';
import { ResearchsubjectComponent } from './researchsubject/researchsubject.component';
import { ResearchsubjectDetailComponent } from './researchsubject-detail/researchsubject-detail.component';
import { DataService } from './data.service';
import {
  OwlDateTimeModule,
  OwlNativeDateTimeModule,
} from '@danielmoncada/angular-datetime-picker';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

@NgModule({
  declarations: [
    AppComponent,
    PatientsComponent,
    PatientDetailComponent,
    SidebarComponent,
    PractitionersComponent,
    PractitionerDetailComponent,
    FlagsComponent,
    FlagDetailComponent,
    PractitionerRolesComponent,
    PractitionerRoleDetailComponent,
    ResearchsubjectComponent,
    ResearchsubjectDetailComponent,
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  providers: [
    DataService,
    // use french locale
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'de' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
